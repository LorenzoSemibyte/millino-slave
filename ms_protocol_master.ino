#include "ms_protocol_master.h"
#include "timer.h"

MsProtocolMaster::MsProtocolMaster() :
  m_connected(false)
{
  m_server = new EthernetServer(SERVER_PORT);
  m_server->begin();
}

MsProtocolMaster& MsProtocolMaster::GetInstance()
{
  static MsProtocolMaster client;
  return client;
}

void MsProtocolMaster::Read()
{
  EthernetClient client;
  char byteRead;
  int bufferIndex = 0;
  Timer t(RESP_TIMEOUT);
  
  while(!t.Timeout())
  {
      client = m_server->available();
      if(!client)
          continue;
      
      if(client.available())
      {
          byteRead = client.read();
          if(byteRead != -1)
           m_buffer[bufferIndex++] = byteRead;
      }
  }
}

void MsProtocolMaster::Write(String command)
{
   byte written = m_client.write(command.c_str(), command.length());

    if(written != command.length())
    {
        Serial.print("Connessione fallita");
        //ADD_ERROR("", ERROR_CONNECTION);
    }
}

void MsProtocolMaster::SetMachine(int relayDescriptor, bool status)
{
  int opCode = SET_MACHINE; 
  int arg2 = status ? 1 : 0;
  Timer t(2000);

  while(!t.Timeout() && !IsConnected())
    Connect();

  if(!IsConnected())
  {
    ADD_ERROR("", ERROR_CONNECTION);
    return;
  }

  String command = m_serializer.Serialize(opCode, relayDescriptor, arg2);
  Write(command);
  Disconnect();
}

bool MsProtocolMaster::GetMachineStatus(int pinDescriptor)
{
  int opCode = GET_STATUS;
  Timer t(2000);

  while(!t.Timeout() && !IsConnected())
    Connect();
  
  if(!IsConnected())
  {
    ADD_ERROR("", ERROR_CONNECTION);
    return;
  }

  String command = m_serializer.Serialize(opCode, pinDescriptor);
  Write(command);
  Disconnect();

  Read();
  RecvResponse recvResponse = m_deserializer.Deserialize(*m_buffer);
  return recvResponse.Status == 1 ? true : false;
}

void MsProtocolMaster::Connect()
{
    byte serverIp[] = {192, 168, 0, 1};
    m_connected = m_client.connect(serverIp, SERVER_PORT) == 1 ? true : false;
}

void MsProtocolMaster::Disconnect()
{
    if(m_connected)
    {
        m_client.stop();
        m_connected = false;
    }
}
