#ifndef _MILLINO_MACHINE_H_
#define _MILLINO_MACHINE_H_

#include "pin.h"
#include "relay.h"
#include "ms_protocol_master.h"

enum class Action
{
  POWER_ON,
  POWER_OFF    
};

enum class SensorStatus 
{
  SENSOR_ON,
  SENSOR_OFF,
  NO_SENSOR
};

enum class MachineType
{
  MACHINE,
  FAN,
  ELEVATOR,
  SHUTTER
};

class Machine
{
   
public:
    Machine(const String& name, const Relay& relay, const Pin& statusPin, const Pin& sensor, const int timeout = 0, bool isOnSlave = false);
    Machine(const String& name, const Relay& relay, const Pin& statusPin, const int timeout = 0, bool isOnSlave = false);

    void DoAction(const Action action);

    String GetName() const { return m_name;}

    bool GetStatus();

    SensorStatus GetSensorStatus();
    
    int GetTimeout() const { return m_timeout;}

    bool HasSensor() const {return m_sensor.IsValid();}

    bool isOnSlave() const {return m_isOnSlave;}

    int GetRelayDescriptor() const { return m_relay.GetDescriptor(); }
    int GetPinDescriptor() const { return m_statusPin.GetDescriptor(); }
    int GetSensorDescriptor() const { return m_sensor.GetDescriptor(); }
    

private:
    int    m_timeout;
    String m_name;
    Relay  m_relay;
    Pin    m_statusPin;
    Pin    m_sensor;
    bool   m_isOnSlave;
};

extern const int seqOneMachinesAmount;
extern const int seqOneElevatorAmount;
extern const int seqOneFansAmount;

extern const int seqTwoMachinesAmount;
extern const int seqTwoElevatorAmount;
extern const int seqTwoFansAmount;

extern const int seqThreeMachinesAmount;
extern const int seqThreeElevatorAmount;
extern const int seqThreeFansAmount;

extern const int seqFourMachinesAmount;
extern const int seqFourElevatorAmount;
extern const int seqFourFansAmount;

extern Machine seqOneFans      [];
extern Machine seqOneElevators [];
extern Machine seqOneMachines  [];

extern Machine seqTwoFans      [];
extern Machine seqTwoElevators [];
extern Machine seqTwoMachines  [];

extern Machine seqThreeFans      [];
extern Machine seqThreeElevators [];
extern Machine seqThreeMachines  [];

extern Machine seqFourFans      [];
extern Machine seqFourElevators [];
extern Machine seqFourMachines  [];

#endif //MACHINE_H 
