#include "ms_slave_deserializer.h"
#include "timer.h"
#include "ms_protocol_config.h"

MsSlaveDeserializer::MsSlaveDeserializer() :
m_server(SERVER_PORT)
{
    m_server.begin(); 
}

RecvCommand MsSlaveDeserializer::Deserialize(char* recvBuffer)
{
    RecvCommand recvCommand;

    recvCommand.OpCode = int(recvBuffer[CMD_OP_CODE]) - 48;
    recvCommand.Arg1   = (int(recvBuffer[CMD_DIGIT_1_ARG_1])   - 48) * 10 + int(recvBuffer[CMD_DIGIT_2_ARG_1]) - 48;
    recvCommand.Arg2   = int(recvBuffer[CMD_ARG_2])   - 48;

    // Serial.println("opcode:" + (String) recvCommand.OpCode);
    // Serial.println("arg1:" + (String) recvCommand.Arg1);
    // Serial.println("arg2:" + (String) recvCommand.Arg2);

    return recvCommand;
}
