// #ifndef DISPLAY_STATUS
// #define DISPLAY_STATUS

// #include "config.h"

// class DisplayStatus
// {
// public:
//   DisplayStatus(Sequence avSequences[]);
  
//   void SetInfo(String text)          {m_info = text;}
//   void SetSelected(unsigned int idx) {m_selected = idx;}               
//   void AddMsg(const String msg);
//   void RemoveMsg(const String msg);
//   void MoveMsgs();
//   void IncrementRtcCursorIndex(); 

//   String       Info()              {return m_info;}
//   String       Selected()          {return m_sequences[m_selected].Name();}
//   uint16       SelectedIdx()       {return m_selected;}
//   String       DisplayedMsg()      {return m_msgs[m_displayedMsg];}
//   uint16       MsgCount()          {return m_msgCount;}

// private:
// 	bool          m_configuringRtc;
//   String        m_title;
//   String        m_info; 
// 	Sequence*     m_sequences;
//   String        m_msgs[DISPLAY_MAX_MSG];
//   uint16        m_selected;
//   uint16        m_displayedMsg;
//   uint16        m_msgCount;
//   uint16        m_rtcCursorIndex;

// };

// #endif //DISPLAY_STATUS
