#include "machine.h"
//#include "config.h"

// TODO indentare a modo
// Sequenza 1
const int seqOneMachinesAmount = 8;
const int seqOneElevatorAmount = 10;
const int seqOneFansAmount     = 6;

Machine seqOneFans [] =
{
  Machine("16-ciclone",  Relay(CICLONE_D9_PIN_C2),   Pin(PULITORE_A9_PIN)),
  Machine("18-ciclone",  Relay(CICLONE_D11_PIN_C2),  Pin(PULITORE_A9_PIN)),
  Machine("24-ciclone",  Relay(CICLONE_D13_PIN_C2),  Pin(PULITORE_A9_PIN)),
  Machine("15-stellare", Relay(STELLARE_D8_PIN_C2),  Pin(PULITORE_A9_PIN)),
  Machine("17-stellare", Relay(STELLARE_D10_PIN_C2), Pin(PULITORE_A9_PIN)),
  Machine("25-stellare", Relay(STELLARE_D12_PIN_C2), Pin(PULITORE_A9_PIN))  
};

Machine seqOneElevators [] =
{
  Machine("01-elevatore",  Relay(ELEVATORE_R0_PIN),     Pin(PULITORE_A9_PIN)),
  Machine("03-elevatore",  Relay(ELEVATORE_R2_PIN),     Pin(PULITORE_A9_PIN)),
  Machine("05-elevatore",  Relay(ELEVATORE_R4_PIN),     Pin(PULITORE_A9_PIN)),
  Machine("07-elevatore",  Relay(ELEVATORE_R6_PIN),     Pin(PULITORE_A9_PIN)),
  Machine("09-elevatore",  Relay(ELEVATORE_R8_PIN),     Pin(PULITORE_A9_PIN)),
  Machine("11-elevatore",  Relay(ELEVATORE_R10_PIN),    Pin(PULITORE_A9_PIN)),
  Machine("13-elevatore",  Relay(ELEVATORE_R12_PIN),    Pin(PULITORE_A9_PIN)),
  Machine("13b-elevatore", Relay(ELEVATORE_R12_PIN_C2), Pin(PULITORE_A9_PIN)),
  Machine("20-elevatore",  Relay(ELEVATORE_R4_PIN_C2),  Pin(PULITORE_A9_PIN)),
  Machine("21-elevatore",  Relay(ELEVATORE_R5_PIN_C2),  Pin(PULITORE_A9_PIN))
  
};

Machine seqOneMachines [] =
{
  Machine("14-selottica",    Relay(SEL_OTTICA_R14_PIN),     Pin(PULITORE_A9_PIN), 1000),
  Machine("08-sbramino",     Relay(SBRAMINO_R0_PIN_C2),     Pin(PULITORE_A9_PIN), 1000, true),
  Machine("02-pulitore",     Relay(PULITORE_R1_PIN),        Pin(PULITORE_A9_PIN), 1000),
  Machine("04-cilindro",     Relay(CILINDRO_R3_PIN),        Pin(PULITORE_A9_PIN), 1000),
  Machine("06-spietratore",  Relay(SPIETRATORE_R5_PIN),     Pin(PULITORE_A9_PIN), 1000),
  Machine("10-tarara",       Relay(TARARA_R1_PIN_C2),       Pin(PULITORE_A9_PIN), 1000),
  Machine("12-tavolagrav",   Relay(TAVOLA_GRAV_R2_PIN_C2),  Pin(PULITORE_A9_PIN), 1000),
  Machine("19-calibratrice", Relay(CALIBRATRICE_R3_PIN_C2), Pin(PULITORE_A9_PIN), 1000)  
};

// Sequenza 2
const int seqTwoMachinesAmount = 1;
const int seqTwoElevatorAmount = 1;
const int seqTwoFansAmount     = 1;

Machine seqTwoFans [] =
{
  Machine("02-fan", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), Pin(SENSOR_2_PIN), 1100)
};

Machine seqTwoElevators [] =
{
  Machine("03-el", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), Pin(SENSOR_2_PIN), 1100)
};

Machine seqTwoMachines [] =
{
  Machine("04-mach", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), Pin(SENSOR_2_PIN), 1100)
};


// Sequenza 3
const int seqThreeMachinesAmount = 1;
const int seqThreeElevatorAmount = 1;
const int seqThreeFansAmount     = 1;

Machine seqThreeFans [] =
{
  Machine("04-fan", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), 1000)
};

Machine seqThreeElevators [] =
{
  Machine("04-elev", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), 1000)
};

Machine seqThreeMachines [] =
{
  Machine("0399-mach", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), 1000)
};


// Sequenza 4
const int seqFourMachinesAmount = 1;
const int seqFourElevatorAmount = 1;
const int seqFourFansAmount     = 1;

Machine seqFourFans [] =
{
  Machine("04-fan888", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), 1000)
};

Machine seqFourElevators [] =
{
  Machine("04-elev7777", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), 1000)
};

Machine seqFourMachines [] =
{
  Machine("04-mach-999", Relay(PULITORE_R1_PIN), Pin(PULITORE_A9_PIN), 1000)
};

 Machine::Machine(const String& name, const Relay& relay, const Pin& statusPin, const Pin& sensor, const int timeout, bool isOnSlave) :
 m_name(name),
 m_relay(relay),
 m_statusPin(statusPin),
 m_sensor(sensor),
 m_timeout(timeout),
 m_isOnSlave(isOnSlave)
 {
 }

Machine::Machine(const String& name, const Relay& relay, const Pin& statusPin, const int timeout, bool isOnSlave) :
 m_name(name),
 m_relay(relay),
 m_statusPin(statusPin),
 m_sensor(Pin(-1)),
 m_timeout(timeout),
 m_isOnSlave(isOnSlave)
 {
 }

 void Machine::DoAction(const Action action)
{ 
  bool status = action == Action::POWER_ON ? true : false;

  if(m_isOnSlave)
    SET_MACHINE_ON_SLAVE(GetRelayDescriptor(), status);
  else
    m_relay.Write(status);
}

bool Machine::GetStatus()
{
  bool status;

  if(m_isOnSlave)
    status = GET_STATUS_ON_SLAVE(GetPinDescriptor());
  else
    status = true;// m_statusPin.IsOn();
  
  return status;
}

SensorStatus Machine::GetSensorStatus()
{
  SensorStatus status;
  if(!m_sensor.IsValid())
    return SensorStatus::NO_SENSOR;
  
  if(m_isOnSlave)
    status = GET_STATUS_ON_SLAVE(GetSensorDescriptor()) ? SensorStatus::SENSOR_ON : SensorStatus::SENSOR_OFF;
  else
    status = m_sensor.IsOn() ? SensorStatus::SENSOR_ON : SensorStatus::SENSOR_OFF;

  return status;
}
