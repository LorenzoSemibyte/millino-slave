#ifndef _MILLINO_RELAY_H
#define _MILLINO_RELAY_H

class Relay
{
  public:
    Relay(int descriptor);
    void Write(bool arg);
    int  GetDescriptor() const { return m_descriptor; }
    
  private:
    int m_descriptor;
};

#endif //RELAY_H 
