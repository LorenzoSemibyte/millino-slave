#ifndef MILLINO_CONTROLLER_H
#define MILLINO_CONTROLLER_H

#include "sequence.h"
#include "config.h"

class MillinoController
{
public:
  MillinoController(Sequence sequenceList[]);

  void SelectNextSequence();
  void SelectPreviousSequence();
  void StartSelectedSequence();
  void StopCurrentSequence();
  void SetStatus(const Status status) {m_status = status;}
  
  
  void ManageDisplay();
  bool IsRunning() {};

private:
  bool CheckSequence(); // TODO schifo, rinominare
  // void ManageSensors(); 
  void ManageMachineStatus();

private: 
  Status    m_status; 
  Sequence* m_currentSequence; //  globale, sempre viva
  Sequence* m_sequenceList;
  int       m_sequenceIndex;
};

#endif //MILLINO_CONTROLLER_H
