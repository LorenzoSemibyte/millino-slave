#include "user_input.h"

UserInput::UserInput() :
m_btnOk(BTN_OK),
m_btnStop(BTN_STOP_PIN),
m_btnUp(BTN_UP),
m_btnDown(BTN_DOWN)
{
}

UserInput& UserInput::GetInstance()
{
  static UserInput instance;
  return instance;
}

bool UserInput::OkPressed() const
{
  return m_btnOk.IsOn();
}

bool UserInput::StopPressed() const
{
  return m_btnStop.IsOn();
}

bool UserInput::UpPressed() const
{
  return m_btnUp.IsOn();
}

bool UserInput::DownPressed() const
{
  return m_btnDown.IsOn();
}
