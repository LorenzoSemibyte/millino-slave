#include "millno_controller.h"

MillinoController::MillinoController(Sequence* sequenceList) :
 m_sequenceIndex(0)
{
  m_sequenceList = sequenceList;
  m_currentSequence = &m_sequenceList[0];
  GeneralStatus::GetInstance().SetCurrentSequence(m_currentSequence->GetName());
}

void MillinoController::SelectNextSequence()
{
 if(++m_sequenceIndex > 3)
   m_sequenceIndex = 0;

 m_currentSequence = &m_sequenceList[m_sequenceIndex];  
 GeneralStatus::GetInstance().SetCurrentSequence(m_currentSequence->GetName());
}

void MillinoController::SelectPreviousSequence()
{
 if(--m_sequenceIndex < 0)
   m_sequenceIndex = 3;

 m_currentSequence = &m_sequenceList[m_sequenceIndex];  
 GeneralStatus::GetInstance().SetCurrentSequence(m_currentSequence->GetName());
}

void MillinoController::StartSelectedSequence()
{
  m_currentSequence->Start();
}

void MillinoController::StopCurrentSequence()
{
  m_currentSequence->PowerOff();
}