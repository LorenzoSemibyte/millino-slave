#ifndef _MS_MASTER_SERIALIZER_H_
#define _MS_MASTER_SERIALIZER_H_

#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>

class MsMasterSerializer
{
    public:
      MsMasterSerializer();
      String Serialize(int opcode, int arg1 = 0, int arg2 = 0);
};

#endif //_MS_MASTER_SERIALIZER_H_