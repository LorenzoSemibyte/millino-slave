#ifndef __MILLINO_TYPES_H__
#define __MILLINO_TYPES_H__

// Arduino unsigned integers are equals, in size, to signed integers
// The only difference is that the latter won't store negative numbers

typedef unsigned int       uint16;
typedef unsigned long int  uint32;

#endif //TYPES_H
