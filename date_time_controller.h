// #ifndef DATE_TIME_CONFIGURER_H
// #define DATE_TIME_CONFIGURER_H

// #include "types.h"
// #include "config.h"

// class DateTimeConfigurer
// {
// public:
//   DateTimeConfigurer(DisplayStatus& status);

//   void IncrementCurrentField();
//   void DecrementCurrentField();
//   void SelectNextField();
//   void ApplyConfig();

// private:
//   bool IsYearBissextile();
//   void AdjustDayValue();
//   void AdjustMonthValue();
//   void AdjustYearValue();
//   void AdjustHourValue();
//   void AdjustMinuteValue();  
//   void LimitsCheck();

//   uint16        m_d;
//   uint16        m_m;
//   uint16        m_y;
//   uint16        m_h;
//   uint16        m_min;
//   uint16        m_currentField;
//   DisplayStatus m_displayStatus;
//   uint16*       m_fieldList[DATE_TIME_FIELDS] = {&m_d, &m_m, &m_y, &m_h, &m_min};
// };

// #endif //DATE_TIME_CONFIGURER_H
