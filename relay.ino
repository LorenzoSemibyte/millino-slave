#include "relay.h"

Relay::Relay(int descriptor) :
m_descriptor(descriptor)
{
}

void Relay::Write(bool arg)
{
  digitalWrite(m_descriptor, arg ? HIGH : LOW); 
}