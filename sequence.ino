#include <Controllino.h>
#include "sequence.h"
#include "user_input.h"
#include "timer.h"

Sequence::Sequence(String name, Machine* fansList ,Machine* elevatorList, Machine* machineList,  int fansSize, int elevatorSize, int machineSize) :
m_name(name),
m_fans(fansList),
m_elevators(elevatorList),
m_machines(machineList),
m_fansSize(fansSize),
m_elevatorSize(elevatorSize),
m_machineSize(machineSize)
{
}

bool Sequence::SafeDelay(int timeToWait)
{
  Timer countdown(timeToWait);
  
  while(!countdown.Timeout())
  {
    if(STOP_BTN && STATUS != POWERING_DOWN)
    {
      Serial.println("STOP pressed");
      PowerOff();
      return false;
    }

    // iterazione su ogni macchina(cicloni, macchine, elevatori)
    if(!CheckFans() && STATUS != POWERING_DOWN)
    {
       PowerOff();
       return false;
    }

    if(!CheckElevators() && STATUS != POWERING_DOWN)
    {
      PowerOff();
      return false;
    }

    if(!CheckMachines() && STATUS != POWERING_DOWN)
    {
      PowerOff();
      return false;
    }

    // if(!CheckSensors() && STATUS != POWERING_DOWN)
    // {
    //   if(STATUS != MANAGING_SENSORS && !ManageSensorAlert(STATUS))
    //   {
    //       PowerOff();
    //       returnValue = false;
    //   }
      
    // }
    // else if(STATUS == MANAGING_SENSORS)
    // {
    //   return true;
    // }
    // display.refresh
  }  
  return true;
}

void Sequence::Start()
{
  Serial.println("SEQUENCE STARTED:" + m_name);
  SET_STATUS(Status::STARTING_SEQUENCE);

  if(!CheckFans())
  { 
    Serial.println("FAN ERROR");
    return;
  }
  
  if(!CheckElevators())
  { 
    Serial.println("ELEVATOR ERROR");
    return;
  }
  
  if(!CheckMachines())
  { 
    Serial.println("MACHINE ERROR");
    return;
  }

  if(!StartFans())
  {
    Serial.println("FAN ERROR ON STARTING");
    SET_STATUS(Status::STARTING_SEQUENCE);
    return;
  }
  Serial.println("FANS STARTED");

  if(!SafeDelay(TIMEOUT_CICLONI_ON))
  {
    SET_STATUS(Status::STARTING_SEQUENCE);
    return;
  }

  if(!StartElevators())
  {
    Serial.println("ELEVATOR ERROR ON STARTING");
    SET_STATUS(Status::STARTING_SEQUENCE);
    return;
  }
  Serial.println("ELEVATORS STARTED");

  if(!SafeDelay(TIMEOUT_ELEVATORI_ON))
  {
    SET_STATUS(Status::STARTING_SEQUENCE);
    return;
  }

  if(!StartMachines())
  {
    Serial.println("MACHINE ERROR ON STARTING");
    SET_STATUS(Status::STARTING_SEQUENCE);
    return;
  }
  Serial.println("MACHINES STARTED");
  
  SET_STATUS(SEQUENCE_RUNNING);
}

void Sequence::PowerOff(bool instant)
{
  SET_STATUS(POWERING_DOWN);

  TurnOffMachines(instant);   //Tempo di esecuzione non trascurabile
  Serial.println("machines off");
  SafeDelay(instant ? 10 : 1000);

  TurnOffElevators();  //Tempo di esecuzione  trascurabile
  Serial.println("elevators off");
  SafeDelay(instant ? 10 : 1000);

  TurnOffFans();       //Tempo di esecuzione  trascurabile
  Serial.println("fans off");
  SET_STATUS(SELECTING_SEQUENCE);
}

void Sequence::PowerOnBeforeSensor(int sensor)
{

  for(int i = 0; i < m_elevatorSize; i++)
  {
    m_elevators[i].DoAction(Action::POWER_ON);
  }
 
  SafeDelay(TIMEOUT_ELEVATORI_ON);
  
  for(int i = 0; i < sensor; i++)
  {
    m_machines[i].DoAction(Action::POWER_ON);
  }
  
}

void Sequence::PowerOffBeforeSensor(int sensor)
{
  for(int i=sensor - 1; i >= 0 ; i--)
    m_machines[i].DoAction(Action::POWER_OFF);
  
  SafeDelay(TIMEOUT_ELEVATORI_OFF);

  for(int i=sensor - 1; i >= 0 ; i--)
    m_elevators[i].DoAction(Action::POWER_OFF);
}

// void Sequence::GetMachinesStatus(MachineStatus* status)
// {  
//   for(int i= 0; i < m_machineSize; i++)
//   {
//     status[i].name =         m_machines[i].GetName();
//     status[i].statusOk =     m_machines[i].GetStatus();
//     status[i].sensorStatus = m_machines[i].GetSensorStatus();
//   }

// }

// void Sequence::GetFansStatus(MachineStatus* status)
// {  
//   for(int i= 0; i < m_fansSize; i++)
//   {
//     status[i].name =         m_machines[i].GetName();
//     status[i].statusOk =     m_machines[i].GetStatus();
//   }

// }

// void Sequence::GetElevatorStatus(MachineStatus* status)
// {  
//   for(int i= 0; i < m_elevatorSize; i++)
//   {
//     status[i].name     = m_machines[i].GetName();
//     status[i].statusOk = m_machines[i].GetStatus();
//   }

// }

// void Sequence::CheckSequence()
// {
//   // check fans status
//   // check elevators status
//   // check machines status
//   // check sensors status
// }

bool Sequence::CheckFans()
{
for(int i = 0; i < m_fansSize; i++)
 {
   if(!m_fans[i].GetStatus())
   {
     ADD_ERROR(m_fans[i].GetName(), ERROR_FAN_STATUS);
     return false;
   }
 }

 return true;
}

bool Sequence::CheckElevators()
{
for(int i = 0; i < m_elevatorSize ; i++)
 {
   if(!m_elevators[i].GetStatus())
  {
    ADD_ERROR(m_elevators[i].GetName(), ERROR_ELEVATOR_STATUS);
    return false;
  }
 }

 return true;
}

bool Sequence::CheckMachines()
{
for(int i = 0; i < m_machineSize ; i++)
 {
   // if(!SafeDelay(m_machines[i].GetTimeout()))
   //  {
   //    return false;
   //    // boh
   //  }

   if(!m_machines[i].GetStatus())
   {
     ADD_ERROR(m_machines[i].GetName(), ERROR_MACHINE_STATUS);
     return false;
   }
 }
  return true;
}

bool Sequence::CheckSensors()
{
  int lastTriggeredSensor = -1;
  bool retValue = true; 

  for(int i = 0; i < m_machineSize; i++)
  {
    if(m_machines[i].HasSensor() && m_machines[i].GetSensorStatus() == SensorStatus::SENSOR_ON)
    {
      ADD_ERROR(m_machines[i].GetName(), ErrorType::ERROR_TRIGGERED_SENSOR);
      lastTriggeredSensor = i;
    }

    if(lastTriggeredSensor != -1)
    {
      SET_LAST_SENSOR_TRIGGERED(lastTriggeredSensor);
      PowerOffBeforeSensor(lastTriggeredSensor);
      // if(STATUS != SENSORS_TRIGGERED)
      //   retValue = ManageSensorAlert();
    }

  }

  return retValue;
}

bool Sequence::StartFans()
{
  for(int i = 0; i < m_fansSize; i++)
  {
    if(m_fans[i].GetStatus())
    {    
      m_fans[i].DoAction(Action::POWER_ON);
      Serial.println(m_fans[i].GetName() + " started");
    }
    else
    {     
      ADD_ERROR(m_fans[i].GetName(), ERROR_FAN_STATUS);
      PowerOff();
      return false;
    }
  }
  return true;
}

bool Sequence::StartElevators()
{
   for(int i = 0; i < m_elevatorSize; i++)
  {
    if(m_elevators[i].GetStatus())
    {
      m_elevators[i].DoAction(Action::POWER_ON);
      Serial.println(m_elevators[i].GetName() + " started");
    }
    else
    {
      ADD_ERROR(m_elevators[i].GetName(), ERROR_ELEVATOR_STATUS);
      PowerOff();
      return false;
    }
  }

  return true;
}

bool Sequence::StartMachines()
{
   for(int i = 0; i < m_machineSize; i++)
  {
    if(m_machines[i].GetStatus())
    {
      if(!SafeDelay(m_machines[i].GetTimeout()))
        {
          SET_STATUS(Status::SELECTING_SEQUENCE);
          return false;
        }

      m_machines[i].DoAction(Action::POWER_ON);
      Serial.println(m_machines[i].GetName() + " started");
    }
    else
    {
      ADD_ERROR(m_machines[i].GetName(), ERROR_MACHINE_STATUS);
      PowerOff();
      return false;
    }
  }

  return true;
}

void Sequence::TurnOffMachines(bool instant)
{
  for(int i = m_machineSize - 1; i >= 0; i--)
  {
    if(!instant)
    {
      SafeDelay(m_machines[i].GetTimeout());
    }
       

    //if(instant && !m...isOnSlave)
    m_machines[i].DoAction(Action::POWER_OFF);
    // if(instnat)
      // turnoff all
  }
}

void Sequence::TurnOffElevators()
{
  for(int i = m_elevatorSize - 1; i >= 0; i--)
  {
    m_elevators[i].DoAction(Action::POWER_OFF);
  }
}

void Sequence::TurnOffFans()
{
  for(int i = m_fansSize - 1; i >= 0; i--)
  {
    m_fans[i].DoAction(Action::POWER_OFF);
  }
}

bool Sequence::ManageSensorAlert(Status lastStatus)
{
  // Timer t(30000);
  SET_STATUS(MANAGING_SENSORS);
  
  if(SafeDelay(30000))
  {
    SET_STATUS(lastStatus);
    PowerOnBeforeSensor(LAST_SENSOR_TRIGGERED);
    return true;
  }
  else
  {
    SET_STATUS(SELECTING_SEQUENCE);
    return false;
  }

// /////////////
//   while(!CheckSensors())
//   {
//     CheckFans();

//     CheckElevators();

//     CheckMachines();

//     if(Timer.Timeout())
//     {
//       SET_STATUS(SELECTING_SEQUENCE);
//       return false;
//     }
//   }

//   return true;
}
