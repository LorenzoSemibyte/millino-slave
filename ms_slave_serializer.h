#ifndef _MS_SLAVE_SERIALIZER_H_
#define _MS_SLAVE_SERIALIZER_H_

#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>

class MsSlaveSerializer
{
    public:
      MsSlaveSerializer();
      void Serialize(int status);
      bool IsConnected() {return m_connected; }
      void Connect();
      void Disconnect();
    
    private:
       EthernetClient m_client;
       bool           m_connected;
};

#endif //_MS_SLAVE_SERIALIZER_H_