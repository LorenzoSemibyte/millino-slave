#include "ms_protocol_slave.h"
#include "ms_protocol_config.h"
#include "pin.h"
#include "relay.h"

MsProtocolSlave::MsProtocolSlave() :
 m_ethServer(SERVER_PORT)
{
 m_ethServer.begin();
}

MsProtocolSlave& MsProtocolSlave::GetInstance()
{
  static MsProtocolSlave server;
  return server;
}

void MsProtocolSlave::Init()
{
}

bool MsProtocolSlave::Listen()
{
  return true;
  // while(true)
  // {
  //   RecvCommand recvCommand = m_deserializer.Deserialize();
  //   int opCode = recvCommand.OpCode;

  //   if(opCode == SET_MACHINE)
  //   {
  //     int relayDescriptor = recvCommand.Arg1;
  //     int status = recvCommand.Arg2;
  //     Relay relay(relayDescriptor);
  //     relay.Write(status);
  //   }
  //   else if(opCode == SET_ALL_OFF)
  //   {
  //     //TODO
  //   }
  //   else if(opCode == GET_ST)
  //   {
  //     int pinDescriptor = recvCommand.Arg1;
  //     Pin pin(pinDescriptor);
  //     SendMachineStatus(pin);
  //   }
  // }
}

bool MsProtocolSlave::IsMasterConnected()
{
  EthernetClient client;
  client = m_ethServer.available();
  
  if(client.connected())
  {
    m_ethClient = client;
    return true;
  }
  else
  {
    return false;
  }
 }

void MsProtocolSlave::ProcessCommand()
{
  Read();

  RecvCommand cmd = m_deserializer.Deserialize(m_buffer);

  switch(cmd.OpCode)
  {
    case SET_MACHINE:
    this->SetMachine(cmd.Arg1, cmd.Arg2);
    break;
    case GET_STATUS:
    this->SendMachineStatus(cmd.Arg1);
    break;
    default:
    // male
    // alza errore
    break;
  }
}

void MsProtocolSlave::Read()
{
  int bufferIndex = 0;
  char byteRead;

  memset(m_buffer, 0, MSG_MAX_SIZE);

  while(m_ethClient.available())
  {
     byteRead = m_ethClient.read();
     if(byteRead != -1)
     {
       m_buffer[bufferIndex++] = byteRead;
     }
  }
  Serial.println("read:" + (String) m_buffer);
}

void MsProtocolSlave::SetMachine (int descriptor, int status) const
{
  Serial.println("set machine " + (String) descriptor + " to " + (String) status);
  Relay relay(descriptor);
  relay.Write((status > 0) ? true : false);
  m_ethClient.stop();

}

void MsProtocolSlave::SendMachineStatus(int descriptor)
{
  Pin statusPin(descriptor);
  
  int written = m_ethServer.write(/*statusPin.IsOn() ? "1" : "0"*/"1", 1); 
  Serial.println("Written " + (String) written);
  m_ethClient.stop();
}

