#ifndef _MS_PROTOCOL_MASTER_H_
#define _MS_PROTOCOL_MASTER_H_

#include "machine.h"
#include "ms_master_serializer.h"
#include "ms_master_deserializer.h"
#include "ms_protocol_config.h"

#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>

#define SET_MACHINE_ON_SLAVE(relay, status) MsProtocolMaster::GetInstance().SetMachine(relay, status)
#define GET_STATUS_ON_SLAVE(pin)          MsProtocolMaster::GetInstance().GetMachineStatus(pin)

class MsProtocolMaster
{
	
  public:
    static MsProtocolMaster& GetInstance();
    void SetMachine(int relayDescriptor, bool status);
    bool GetMachineStatus(int pinDescriptor);
    bool IsConnected() {return m_connected; }
    void Connect();
    void Disconnect();
  
  
  private:
	  MsProtocolMaster();
	  MsProtocolMaster(const MsProtocolMaster&)    = delete;
	  void operator = (const MsProtocolMaster&) = delete;
    void Read();
    void Write(String command);

  private:
    MsMasterSerializer   m_serializer;
    MsMasterDeserializer m_deserializer;
    EthernetServer*       m_server;
    EthernetClient       m_client;
    bool                 m_connected;
    char                 m_buffer[MSG_MAX_SIZE];
};

#endif //_MS_PROTOCOL_MASTER_H_
