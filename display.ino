#include "display.h"

Display& Display::GetInstance()
{
  static Display display;
  return display;
}

void Display::Refresh()
{
  Message msg = GetMessage();
  
  WriteLine(1, msg.Line1);
  WriteLine(2, msg.Line2);
  WriteLine(3, msg.Line3);
  WriteLine(4, msg.Line4);
  
  Serial.print("Current Status: ");

  switch(STATUS)
  {
    case SELECTING_SEQUENCE: Serial.println("SELECTING_SEQUENCE"); break;
    case STARTING_SEQUENCE: Serial.println("STARTING_SEQUENCE"); break;
    case POWERING_DOWN:    Serial.println("POWERING_DOWN"); break;
    case SEQUENCE_RUNNING: Serial.println("SEQUENCE_RUNNING"); break;
    case MANAGING_SENSORS: Serial.println("MANAGING_SENSORS"); break;
    case SENSORS_TRIGGERED: Serial.println("SENSORS_TRIGGERED"); break;
    default: Serial.println("WTF"); break;
  }
}

Message Display::GetMessage()
{
  Message msg;
  msg.Line1 = DISPLAY_TITLE;

  if(STATUS == SELECTING_SEQUENCE)
  {
    msg.Line2 = TEXT_SELECT_SEQUENCE;
    msg.Line3 = CURRENT_SEQUENCE;
    msg.Line4 = TEXT_SELECT_INSTRUCTIONS;
  }
  else if(STATUS == POWERING_DOWN)
  {
    msg.Line2 = CURRENT_SEQUENCE;
    msg.Line3 = TEXT_POWER_DOWN;
    msg.Line4 = TEXT_POWER_DOWN_INSTRUCTIONS;
  }
  else
  {
    msg.Line2 = CURRENT_SEQUENCE;
    msg.Line3 = TEXT_SEQUENCE_RUNNING;
    msg.Line4 = TEXT_SEQUENCE_RUNNING_INSTRUCTIONS;
  }
  return msg;
}

void Display::WriteLine(int n_line, String text)
{
  // TODO
}

