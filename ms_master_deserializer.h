#ifndef _MS_MASTER_DESERIALIZER_H_
#define _MS_MASTER_DESERIALIZER_H_

#include "ms_protocol_config.h"

class MsMasterDeserializer
{
    public:
      MsMasterDeserializer();
      RecvResponse Deserialize(char* buffer);
};

#endif //_MS_MASTER_DESERIALIZER_H_
