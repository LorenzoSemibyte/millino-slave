#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "machine.h"
#include "types.h"
#include "general_status.h"

class Sequence
{
public:
  Sequence(String name, Machine* fansList, Machine* elevatorList, Machine* machineList,  int fansSize, int elevatorSize, int machineSize);

  bool SafeDelay(int timeToWait);
  void Start();
  void PowerOff(bool instant = 0);

  
  // void GetMachinesStatus  (MachineStatus* status) const;
  // void GetFansStatus      (MachineStatus* status) const;
  // void GetElevatorStatus  (MachineStatus* status) const;

  String GetName()      const {return m_name;}
  int GetMachineSize()  const {return m_machineSize;}
  int GetFansSize()     const {return m_fansSize;}
  int GetElevatorSize() const {return m_elevatorSize;}

private:
  // bool CheckSequence(); // TODO schifo, rinominare
  bool CheckFans();
  bool CheckElevators();
  bool CheckMachines();
  bool CheckSensors();
  bool ManageSensorAlert(Status lastStatus);

  bool StartFans();
  bool StartElevators();
  bool StartMachines();

  void TurnOffMachines(bool instant = 0);
  void TurnOffElevators();
  void TurnOffFans();

  void PowerOnBeforeSensor (int sensorIndex);
  void PowerOffBeforeSensor(int sensorIndex);

private:
  String       m_name;
  Machine*     m_machines;
  Machine*     m_elevators;
  Machine*     m_fans;
  
  int          m_machineSize;
  int          m_fansSize;
  int          m_elevatorSize;
  // Sequence*    m_thisObject; // TODO do I really have to do it this way?

  
};

#endif //SEQUENCE_H
