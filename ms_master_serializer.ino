#include "ms_master_serializer.h"
#include "ms_protocol_config.h"

MsMasterSerializer::MsMasterSerializer()
{
}

String MsMasterSerializer::Serialize(int opcode, int arg1, int arg2)
{
    return String(opcode) + (arg1 != 0 ? String(arg1) : "") + (arg2 != 0 ? String(arg2) : "");
   
}