#ifndef _MS_PROTOCOL_CONFIG_H_
#define _MS_PROTOCOL_CONFIG_H_

#include <Ethernet.h>

#define MSG_MAX_SIZE 512

//opcodes
#define SET_MACHINE 0
#define GET_STATUS 2

#define CMD_OP_CODE       0
#define CMD_DIGIT_1_ARG_1 1
#define CMD_DIGIT_2_ARG_1 2
#define CMD_ARG_2         3

#define SERVER_CMD_NOT_INITED -2

#define RESP_STATUS 0

#define RESP_TIMEOUT 500

#define SERVER_PORT 1000

byte masterMac[] = { 0xAE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte masterSubnet[] = {255, 255, 255, 0};
byte masterGateway[] = {192, 168, 0, 2};
IPAddress masterIp(192, 168, 0, 2);

byte slaveMac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte slaveSubnet[] = {255, 255, 255, 0};
byte slaveGateway[] = {192, 168, 0, 2};
IPAddress slaveIp(192, 168, 0, 1);


struct RecvCommand
{
  RecvCommand() :
    OpCode(SERVER_CMD_NOT_INITED),
    Arg1(SERVER_CMD_NOT_INITED),
    Arg2(SERVER_CMD_NOT_INITED)
    {}

  int OpCode;
  int Arg1;
  int Arg2;
};

struct RecvResponse
{
  int Status;
};
#endif //_MS_PROTOCOL_CONFIG_H_
