#include <Controllino.h> 
#include <LiquidCrystal.h>
#include "config.h"
#include "ms_protocol_config.h"
#include "ms_protocol_slave.h"


void setup()
{
  Ethernet.begin(slaveMac, slaveIp, slaveSubnet, slaveGateway);
  Serial.begin(9600);
  Serial.println("Slave started v8.0");
  pinMode(CONTROLLINO_R0, OUTPUT);
  pinMode(CONTROLLINO_R1, OUTPUT);
  pinMode(CONTROLLINO_R2, OUTPUT);
  pinMode(CONTROLLINO_R3, OUTPUT);
  pinMode(CONTROLLINO_R4, OUTPUT);
  pinMode(CONTROLLINO_R5, OUTPUT);
  pinMode(CONTROLLINO_R12, OUTPUT);
  pinMode(CONTROLLINO_R14, OUTPUT);

  pinMode(CONTROLLINO_D8, OUTPUT);
  pinMode(CONTROLLINO_D9, OUTPUT);
  pinMode(CONTROLLINO_D10, OUTPUT);
  pinMode(CONTROLLINO_D11, OUTPUT);
  pinMode(CONTROLLINO_D12, OUTPUT);
  pinMode(CONTROLLINO_D13, OUTPUT);


  //TODO pinMode dei pin rimanenti
}

void loop()
{

  if(MsProtocolSlave::GetInstance().IsMasterConnected())
  {
    Serial.println("Connection from master detected");
    MsProtocolSlave::GetInstance().ProcessCommand();
  }
 delay(500);
}
