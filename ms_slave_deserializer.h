#ifndef _MS_SLAVE_DESERIALIZER_H_
#define _MS_SLAVE_DESERIALIZER_H_

#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>

#include "ms_protocol_slave.h"

class MsSlaveDeserializer
{
    public:
      MsSlaveDeserializer();
      RecvCommand Deserialize(char recBuf[]);
    
    private:
      EthernetServer m_server;
};

#endif //_MS_SLAVE_DESERIALIZER_H_
