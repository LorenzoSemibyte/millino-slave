#include "general_status.h"

GeneralStatus::GeneralStatus() :
m_errorSize(0),
m_currentStatus(SELECTING_SEQUENCE),
m_currentSequence("FARRO")
{
}

GeneralStatus& GeneralStatus::GetInstance()
{
  static GeneralStatus instance;
  return instance;
}

void GeneralStatus::AddError(String engine, ErrorType errorType)
{
  MachineError error;

  error.machine = engine;
  error.errorType = errorType;

  if(!ContainError(error))
  {
    m_errors[m_errorSize] = error;
    m_errorSize++;
    Serial.println("Error added: " + (String) m_errorSize + "/" + (String) MAX_ERRORS);
  }
}

bool GeneralStatus::RemoveError(String engine)
{
  bool targetFound = false;
  for(int i = 0; i < m_errorSize; i++)
  {
    if(m_errors[i].machine == engine)
      targetFound = true;

    if(i < m_errorSize - 1 && targetFound )
      m_errors[i] = m_errors[i + 1];
  }
  m_errorSize--;
  return targetFound;
}

void GeneralStatus::RemoveErrorByIndex(int index)
{
  for(int i = index; i < m_errorSize - 1; i++)
    m_errors[i] = m_errors[i + 1];
  
  m_errorSize--;
}

void GeneralStatus::RemoveAllErrors()
{
  m_errorSize = 0;
}

bool GeneralStatus::ContainError(MachineError error)
{
  for(int i = 0; i < m_errorSize; i++)
  {
    if(error.machine == m_errors[i].machine && error.errorType == m_errors[i].errorType)
      return true;
  }
  return false;
}