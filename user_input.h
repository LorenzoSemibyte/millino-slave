#ifndef USER_INPUT_H
#define USER_INPUT_H

#include "types.h"
#include "config.h"
#include "pin.h"

#define OK_BTN   UserInput::GetInstance().OkPressed()
#define STOP_BTN UserInput::GetInstance().StopPressed()
#define UP_BTN   UserInput::GetInstance().UpPressed()
#define DOWN_BTN UserInput::GetInstance().DownPressed()   

class UserInput
{
public:
  static UserInput& GetInstance();
 
  bool OkPressed()   const;
  bool StopPressed() const;
  bool UpPressed()   const;
  bool DownPressed() const;

private:
	  UserInput();
	  UserInput(const UserInput&)    = delete;
	  void operator = (const UserInput&) = delete; 


private:
  static UserInput* m_instance = NULL;
  Pin m_btnOk;
  Pin m_btnStop;
  Pin m_btnUp;
  Pin m_btnDown;
};

#endif //USER_INPUT_H 
