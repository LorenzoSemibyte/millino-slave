#include "ms_slave_serializer.h"
#include "ms_protocol_config.h"

MsSlaveSerializer::MsSlaveSerializer() :
 m_connected(false) 
{
}

void MsSlaveSerializer::Serialize(int status)
{
    String response = String(status);

    byte written = m_client.write(response.c_str(), response.length());

    if(written != response.length())
    {
        Serial.print("Connessione fallita");
        //ADD_ERROR("", ERROR_CONNECTION);
    }
}

void MsSlaveSerializer::Connect()
{
    byte serverIp[] = {192, 168, 0, 2};
    m_connected = m_client.connect(serverIp, SERVER_PORT) == 1 ? true : false;
}

void MsSlaveSerializer::Disconnect()
{
    if(m_connected)
    {
        m_client.stop();
        m_connected = false;
    }
}
