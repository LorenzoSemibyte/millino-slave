#include "ms_master_deserializer.h"
#include "timer.h"
#include "ms_protocol_config.h"

MsMasterDeserializer::MsMasterDeserializer()
{
}

RecvResponse MsMasterDeserializer::Deserialize(char* buffer)
{
    RecvResponse recvResponse;
    recvResponse.Status = int(buffer[RESP_STATUS]) - 48;
    return recvResponse;
}
