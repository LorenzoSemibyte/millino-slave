#ifndef _MS_PROTOCOL_SLAVE_H_
#define _MS_PROTOCOL_SLAVE_H_

#include "machine.h"
#include "ms_protocol_config.h"
#include "ms_slave_serializer.h"
#include "ms_slave_deserializer.h"

class MsProtocolSlave
{
	
  public:
    static MsProtocolSlave& GetInstance();

    void   Init();
    bool   IsMasterConnected();
    void   ProcessCommand();
    bool   Listen();
    void   SendMachineStatus(Pin statusPin);

  private:
	  MsProtocolSlave();

    void operator = (const MsProtocolSlave&) = delete;
    MsProtocolSlave(const MsProtocolSlave&) = delete;


    void Read();
    void SetMachine(int descriptor, int status) const;
    void SendMachineStatus(int descriptor);


  private:
    EthernetServer      m_ethServer;
    EthernetClient      m_ethClient;

    MsSlaveSerializer   m_serializer;
    MsSlaveDeserializer m_deserializer;
    char                m_buffer[MSG_MAX_SIZE];
};

#endif //_MS_PROTOCOL_SLAVE_H_
