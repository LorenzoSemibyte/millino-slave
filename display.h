#ifndef _MILLINO_DISPLAY_H_
#define _MILLINO_DISPLAY_H_

#include "types.h"
#include "general_status.h"

#define REFRESH_DISPLAY Display::GetInstance().Refresh();

struct Message
{
	String Line1;
	String Line2;
	String Line3;
	String Line4;
};

class Display
{
public:
  static Display& GetInstance();
	void Refresh();

private:
	  Display() {};
	  Display(const Display&)    = delete;
	  void operator = (const Display&) = delete;
		Message GetMessage();
		void WriteLine(int n_line, String text);
};

#endif //DISPLAY_H  
