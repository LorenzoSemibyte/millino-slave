#ifndef _MILLINO_TIMER_H_
#define _MILLINO_TIMER_H_

#include "types.h"

class Timer
{
  public:
    Timer(int msdelay);

    bool Timeout();
    void Reset();

  private:
    uint32 m_start;
    uint32 m_end;
    uint32 m_delay;
    uint16 m_a;
};

#endif //TIMER_H
