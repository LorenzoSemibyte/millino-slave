#ifndef _MILLINO_PIN_H_
#define _MILLINO_PIN_H_

#include "types.h"
#include "config.h"

class Pin
{
public:
    Pin(const int descriptor);
    Pin() {};

    bool IsOn()          const {return analogRead(m_descriptor) >= ANALOG_TRESHOLD;}
    bool IsValid()       const {return m_descriptor != INVALID_PIN;}
    int  GetDescriptor() const {return m_descriptor;}

private:
   int m_descriptor;
   
};

#endif //PIN_H 
