#ifndef _MILLINO_STATUS_H_
#define _MILLINO_STATUS_H_

#include "config.h"
#include "display.h"

#define LAST_SENSOR_TRIGGERED            GeneralStatus::GetInstance().GetLastSensorTriggeredIndex()
#define SET_LAST_SENSOR_TRIGGERED(index) GeneralStatus::GetInstance().SetLastSensorTriggeredIndex(index)

#define STATUS                           GeneralStatus::GetInstance().GetCurrentStatus() 
#define SET_STATUS(status)               GeneralStatus::GetInstance().SetCurrentStatus(status)

#define ADD_ERROR(name, errorType)       GeneralStatus::GetInstance().AddError(name, errorType)
#define REMOVE_ERROR(name)               GeneralStatus::GetInstance().RemoveError(name)
#define REMOVE_ALL_ERRORS                GeneralStatus::GetInstance().RemoveAllErrors()

#define CURRENT_SEQUENCE                 GeneralStatus::GetInstance().GetCurrentSequence()


enum  Status
{
	SELECTING_SEQUENCE,
  STARTING_SEQUENCE,
	SEQUENCE_RUNNING, 
	POWERING_DOWN,
	MANAGING_SENSORS,
	SENSORS_TRIGGERED
};

enum  ErrorType
{ 
  ERROR_MACHINE_STATUS,
  ERROR_FAN_STATUS,
	ERROR_ELEVATOR_STATUS,
	ERROR_TRIGGERED_SENSOR,
	ERROR_CONNECTION
};

class GeneralStatus
{
	
public:
  struct MachineError
  {
     ErrorType errorType;
     String    machine;
  };

  static GeneralStatus& GetInstance();

  void AddError(String engine, ErrorType errorType);
	bool RemoveError(String engine);
	void RemoveErrorByIndex(int index);
	void RemoveAllErrors();
	bool ContainError(MachineError error);

  int  GetLastSensorTriggeredIndex() const            { return m_lastSensorTriggeredIndex; }
  void SetLastSensorTriggeredIndex(const int index )  { m_lastSensorTriggeredIndex = index; }

	String       GetCurrentSequence()            { return m_currentSequence; }
	void         SetCurrentSequence(String name) { m_currentSequence = name; }         
	MachineError GetMachineError(int index)      { return m_errors[index]; }
	int          GetErrorSize()                  { return m_errorSize; }
	Status       GetCurrentStatus()              { return m_currentStatus; }
	void         SetCurrentStatus(Status status) { m_currentStatus = status; }
 
private:
	  GeneralStatus();
	  GeneralStatus(const GeneralStatus&)    = delete;
	  void operator = (const GeneralStatus&) = delete;
	  
private:
	  String         m_currentSequence;
    MachineError   m_errors[MAX_ERRORS];
		int            m_errorSize;
    Status         m_currentStatus;
    int            m_lastSensorTriggeredIndex;
};

#endif //STATUS_H
