#include <Controllino.h>
#include "timer.h"


Timer::Timer(int msdelay)
{
  m_start = millis();
  m_end   = m_start + msdelay;
  m_delay = msdelay;
}

// return true if timer has expired
bool Timer::Timeout()
{
  if((m_start < m_end) && (millis() > m_end) )
  {
    return true;
  }
  else // counter wrapped around
  {
    if((m_start > m_end) && ((millis() < m_start) && (millis() > m_end)))
      return true;
  }

  return false;
}

// reset timer
void Timer::Reset()
{
  m_start = millis();
  m_end   = m_start + m_delay;
}
